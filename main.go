package main

import (
	_ "tutorial-go/routers"

	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}
