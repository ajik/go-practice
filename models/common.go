package models

// ResponseWithData ...
type ResponseWithData struct {
	Code    int         `json:"code";valid:"Required"`
	Status  string      `json:"status";valid:"Required"`
	Message string      `json:"message";valid:"Required"`
	Data    interface{} `json:"data"`
}

// add general response with no data ...
type ResponseWithNoData struct {
	Code    int    `json:"code";valid:"Required"`
	Status  string `json:"status";valid:"Required"`
	Message string `json:"message";valid:"Required"`
}
