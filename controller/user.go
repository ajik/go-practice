package controllers

import (
	"net/http"
	"tutorial-go/models"

	"github.com/astaxie/beego"
)

// Operations about Users
type UserController struct {
	beego.Controller
}

// @Title GetUser
// @Description get users
// @Success 200 {int} models.User
// @router / [get]
func (u *UserController) GetUser() {
	var dataResponse models.User
	var response models.ResponseWithData

	dataResponse.Id = 1
	dataResponse.Name = "aji"
	dataResponse.Address = "Tangerang"

	response.Code = http.StatusOK
	response.Status = "success"
	response.Message = ""
	response.Data = dataResponse

	u.Data["json"] = response

	u.ServeJSON()
}
