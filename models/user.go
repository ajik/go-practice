package models

// User ...
type User struct {
	Id      int    `json:"id";valid:"Required"`
	Name    string `json:"name";valid:"Required"`
	Address string `json:"address";valid:"Required"`
}
